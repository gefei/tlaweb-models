------------------------------- MODULE clock -------------------------------

EXTENDS Naturals

VARIABLES hr, period

vars == <<hr, period>>

Init == /\ hr \in 1..12
        /\ period \in {"am", "pm"}
        
Next == /\ hr' = IF hr = 12 THEN 1 ELSE hr + 1 
        /\ period' = IF hr = 11 THEN
                        IF period = "am" THEN "pm" 
                        ELSE "am"
                     ELSE period               
                     
Spec == Init /\ [][Next]_vars /\ WF_vars(Next)        
                 
Invariant == [](hr \in (1..12))

Liveness == []<>(hr = 12)




=============================================================================
\* Modification History
\* Last modified Sun Feb 02 14:34:54 CET 2020 by gefeizhang
\* Created Sat Feb 01 17:19:28 CET 2020 by gefeizhang
